# Makefile for gnokii-artwork (mainly for use with xgnokii)

xgnokiidir = $(shell pkg-config --variable=xgnokiidir xgnokii)
INSTALL_DATA = $(shell which install) -c -m 644

install:
	mkdir -p $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/keyb/keyb_6110.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/keyb/keyb_6150.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/keyb/keyb_5110.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_6110.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_6150.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_6210.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_3310.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_5110.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_3210.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_6250.xpm $(DESTDIR)$(xgnokiidir)/xpm
	$(INSTALL_DATA) xpm/logo/Preview_7110.xpm $(DESTDIR)$(xgnokiidir)/xpm
